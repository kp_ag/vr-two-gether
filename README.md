<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<div align="center">
    <img src="Title.jpg">
</div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h3 align="center">VR Two-gether</h3>
  <p align="justify">
    A cross-platform cooperative game where players collaborate to solve puzzles based on cooperative design patterns. The game features balanced gameplay mechanics and VR/PC affordances to create meaningful cooperation between players and determine player preferences for entertainment and performance through each level's new mechanics.
    <br />
    <div align="center">
    <a href="https://www.youtube.com/embed/SP-q0IC8ORw" target="_blank">View Demo</a>
    ·
    <a href="https://www.youtube.com/embed/IrXNKPPGJZA" target="_blank">Initial prototype</a>
    </div>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

The game consists of two levels, each with tutorials for both players at the beginning and cooperative puzzle challenges throughout. These levels are designed based on a spectrum of different cooperative dynamics and different affordances for the VR and PC. In the first level, players are introduced to core mechanics of movement and traversal, including asymmetric mechanics of parkour abilities for the PC player and teleportation for the VR player. Players have a chance to learn the controls and new mechanics in a short tutorial, then need to work together to overcome cooperative puzzle challenges.

In the second level, asymmetric mechanics of shooting for the VR player and shield for the PC player are introduced. Similar to level one, players first have time to practice the mechanics at their own pace and then play together in cooperative challenges. The game aims to encourage cooperation between the two players by designing challenges based on cooperative dynamics that create meaningful cooperation presented by Ptrick Redding in his GDC talk with the same topic. The PC player, a treasure hunter, is searching for treasure in ancient hidden cities while he/she meets the VR player, a time traveler. The two players have different skills to learn from each other, with the VR player having technology and futuristic knowledge, and the PC player having mythical knowledge of the ancient world. The two players have a mutual give-and-take relation and balanced abilities as the game progresses.

Project Goal: The goal of VR Two-gether is to create a fun and challenging cooperative game that leverages the unique strengths of both VR and PC gameplay. The game is designed to encourage players to work together, learn from each other, and overcome cooperative challenges

### Built With

Major frameworks/libraries used to bootstrap the project.

* Unity 2021.7 LTS
* Meta Quest 2

### Prerequisites

* Windows 10+
* Unity 2021+
* Meta Quest VR Headset; preferable Meta Quest 2.


### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/kp_ag/vr-two-gether.git
   ```
2. Open Project in Unity

3. Play the Project inside Unity or Make a Build

<!-- CONTACT -->
## Contact

* Kalpan Agrawal - agrawal.k@northeastern.edu
* [![LinkedIn][linkedin-shield]][linkedin-url]
* [![Portfolio][portfolioIcon-url]][portfolio-url]

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/kp-ag/
[portfolioIcon-url]: https://img.shields.io/badge/-Portfolio-brightgreen
[portfolio-url]: https://kalpan-ag.github.io/


