using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBehaviour : MonoBehaviour
{
    private bool IsShieldInHand = false;
    private bool shieldActive = false;
    [SerializeField] private Transform shieldInHand;


    void Update()
    {
        if (IsShieldInHand)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (!shieldActive)
                {
                    Debug.Log("Shield is Active");
                    shieldActive = true;
                    gameObject.SetActive(true);
                }
                else
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerPC")
        {
            IsShieldInHand = true;
            gameObject.SetActive(false);
            gameObject.transform.position = shieldInHand.position;
        }
    }
}