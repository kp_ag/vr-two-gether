using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerShooter : MonoBehaviour
{

    public GameObject bulletPrefab;
    public float bulletSpeed;
    public float fireRate = 0.5f;
    private float nextFire = 0.0f;
    public float spread, spreadFactor, reloadTime;
    public int magazineSize;
    int bulletsLeft;
    //public TextMeshProUGUI bulletsInMag, magSize;
    bool reloading;
    public bool activeShooter = false;
    public Transform gunTip;

    private void Awake()
    {
        bulletsLeft = magazineSize;
        gunTip = GameObject.FindGameObjectWithTag("GunTip").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire && !reloading && bulletsLeft > 0 && activeShooter)
        {
            Shoot();
        }

        if ((Input.GetKeyDown(KeyCode.R) && activeShooter) || (Input.GetButton("Fire1") && Time.time > nextFire && !reloading && bulletsLeft == 0) && activeShooter)
        {
            Reload();
        }

       // UpdateUI();
    }
    void Shoot()
    {

        nextFire = Time.time + fireRate;
        float x = Random.Range(-spread * spreadFactor, spread * spreadFactor);
        float y = Random.Range(-spread * spreadFactor, spread * spreadFactor);
        GameObject projectile = Instantiate(bulletPrefab, gunTip.position + gunTip.forward, gunTip.rotation);
        projectile.transform.Rotate(0, 90, 0);
        var _rb = projectile.GetComponent<Rigidbody>();
        Vector3 directionWithSpread = new Vector3(x, y, 0);
        _rb.AddForce(directionWithSpread + gunTip.forward * bulletSpeed, ForceMode.VelocityChange);
        //Debug.Log("Was: " + bulletsLeft);
        bulletsLeft--;
        //Debug.Log("Now: " + bulletsLeft);
        //bulletsInMag.text = bulletsLeft.ToString();

    }

    private void Reload()
    {
        //  Debug.Log("Reloading!");
        reloading = true;
        Invoke("ReloadFinished", reloadTime);
    }

    private void ReloadFinished()
    {
        bulletsLeft = magazineSize;
        reloading = false;
        //bulletsInMag.text = bulletsLeft.ToString();
    }
   /* public void UpdateUI()
    {
        if (activeShooter)
        {
            bulletsInMag.enabled = true;
            magSize.enabled = true;
            bulletsInMag.text = bulletsLeft.ToString();
            magSize.text = magazineSize.ToString();
        }
        else
        {
            bulletsInMag.enabled = false;
            magSize.enabled = false;
        }
    } */
}


