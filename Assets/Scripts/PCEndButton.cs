using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCEndButton : MonoBehaviour
{

    public bool pcActivated = false;
    private bool doorsOpened = false;
    public GameObject leftDoor, rightDoor;

    // // Start is called before the first frame update
    // void Start()
    // {
        
    // }

    // // Update is called once per frame
    // void Update()
    // {
    //     if (pcActivated)
    //     {
    //         openDoors();
    //     }
    // }

    private void OnTriggerEnter(Collider other)
    {
        leftDoor.transform.position = leftDoor.transform.position + new Vector3(0, 0, 1.5f);
        rightDoor.transform.position = rightDoor.transform.position + new Vector3(0, 0, -1.5f);
        doorsOpened = true;    
    }

    // void openDoors()
    // {

    // }
}
