using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnPlayer : MonoBehaviour
{
    public GameObject playerPC, playerVR;
    public Transform respawnPoint;
    public Transform PCrespawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.tag);
        if (other.gameObject.CompareTag("PlayerPC"))
        {
            Debug.Log("PC respawning");
            playerPC.transform.position = PCrespawnPoint.transform.position;
            Physics.SyncTransforms();
        }

        if (other.gameObject.CompareTag("PlayerVR"))
        {
            Debug.Log("VR respawning");
            playerVR.transform.position = respawnPoint.transform.position;
            Physics.SyncTransforms();
        }
    }

}
