using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;

namespace PlayerStateMachine
{
    // Player States
    public enum PlayerState
    {
        idleState,  // Idle animation, Later get creative?
        moveState,  // Movement, Jump, Sprint
        jumpState,  // Either grab ledge or fall
        climbState  // Bi-direction movement, Vertical jump
    }

    public class PlayerInputController : MonoBehaviour
    {
        [SerializeField] private CharacterController playerController;
        [SerializeField] private Transform cameraTransform;

        [SerializeField] private float playerSpeed = 2.0f;
        [SerializeField] private float jumpHeight = 2.0f;
        [SerializeField] private float gravityValue = -9.81f;
        [SerializeField] private float mouseSense = 20f;
        [SerializeField] private float rotationSpeed = 720f;

        private Vector3 playerVelocity;
        private bool groundedPlayer;

        // Events from input system
        private Vector2 movInput = Vector2.zero;
        private Vector2 mouseLook = Vector2.zero;
        private bool hasJumped = false;

        // TO-DO: BETTER IMPLEMENTATION MAYBE?
        bool wasHanging = false;

        // TO-DO: Remove gloabals
        private float xRotation = 0f;
        private float mouseX = 0f;
        private float setClimbPosition = 0f;

        public PlayerState _state;
        public bool isPaused = false;
        public TextMeshProUGUI resumeText;

        private void Start()
        {
            _state = PlayerState.idleState;
        }

        private void Update()
        {
            Debug.Log(_state);

            if (_state == PlayerState.idleState)
            {
                IdleStateAnimation();
            }

            if (_state == PlayerState.moveState)
            {
                // Move Animation

                MoveAction();
            }

            if (_state == PlayerState.jumpState)
            {
                // Jump Animation

                JumpAction();
            }

            if (_state == PlayerState.climbState)
            {
                // Climb Animation

                ClimbAction();
            }

            // Mouse look rotation
            cameraTransform.localRotation = Quaternion.Euler(xRotation, 0, 0);
            this.transform.Rotate(Vector3.up * mouseX);

            // Pause game
            if (Input.GetKeyDown(KeyCode.Escape) && !isPaused)
            {
                isPaused = true;
                Time.timeScale = 0;
            }

            else if (Input.GetKeyDown(KeyCode.Escape) && isPaused)
            {
                isPaused = false;
                Time.timeScale = 1;
            }

            if (isPaused)
            {
                resumeText.enabled = true;
            }

            else
            {
                resumeText.enabled = false;
            }

        }

        void IdleStateAnimation()
        {
            if (_state == PlayerState.idleState)
            {
                // Start idle animation
            }

            // Make player fall if not grounded
            if (!playerController.isGrounded)
            {
                playerVelocity.y += gravityValue * Time.deltaTime;
                playerController.Move(playerVelocity * Time.deltaTime);
            }
        }

        void MoveAction()
        {
            Vector3 moveDirection = (movInput.y * transform.forward) + (movInput.x * transform.right);
            playerController.Move(moveDirection * Time.deltaTime * playerSpeed);

            // Make player fall if not grounded
            if (!playerController.isGrounded)
            {
                playerVelocity.y += gravityValue * Time.deltaTime;
                playerController.Move(playerVelocity * Time.deltaTime);
            }
            else
            {
                // TO-DO: Removable?
                /*if (playerController.isGrounded && playerVelocity.y < 0)
                {
                    playerVelocity.y = 0f;
                }*/
            }
        }

        void JumpAction()
        {
            if (hasJumped && playerController.isGrounded)
            {
                playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            }

            playerVelocity.y += gravityValue * Time.deltaTime;
            playerController.Move(playerVelocity * Time.deltaTime);
        }

        void ClimbAction()
        {
            transform.position = new Vector3(transform.position.x, setClimbPosition, transform.position.z);

            // Restrict to only left and right movements
            Vector3 moveDirection = (movInput.x * transform.right);
            playerController.Move(moveDirection * Time.deltaTime * playerSpeed);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                wasHanging = false;
                _state = PlayerState.jumpState;
            }
        }

        void GetLedgePosition(Transform ledgeTranform)
        {
            setClimbPosition = ledgeTranform.position.y;
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("Ledge Detected");

            // If the player is in air check if its trying to grab the ledge, change into climb state
            if (other.gameObject.tag == "Ledge")
            {
                if (_state == PlayerState.jumpState && !wasHanging)
                {
                    wasHanging = true;
                    _state = PlayerState.climbState;
                    GetLedgePosition(other.gameObject.transform);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            // Will make the player fall
            if (other.gameObject.tag == "Ledge" && wasHanging)
            {
                if (_state == PlayerState.climbState)
                {
                    _state = PlayerState.jumpState;
                }
            }
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            // Change state to move state if not hanging
            if (!wasHanging)
            {
                _state = PlayerState.moveState;
            }
            movInput = context.ReadValue<Vector2>();
        }

        public void OnJump(InputAction.CallbackContext context)
        {
            // Change state to jump state
            _state = PlayerState.jumpState;

            hasJumped = context.action.triggered;
        }

        public void OnMouseLook(InputAction.CallbackContext context)
        {
            mouseLook = context.ReadValue<Vector2>();
            mouseX = mouseLook.x * mouseSense * Time.deltaTime;
            float mouseY = mouseLook.y * mouseSense * Time.deltaTime;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90);
        }  
    }
}


/*public class PlayerInputController : MonoBehaviour
{
    private void Awake()
    {

    }

    private class Idle : MonoBehaviour
    {
        [SerializeField] private CharacterController playerController;
        private Vector3 playerVelocity;

        private void OnEnable()
        {
            // Idle animation
            // GetComponent<Animator>().Play("Idle");
        }

        private void Update()
        {
            // Restrict the value to negative?
            if (playerController.isGrounded && playerVelocity.y<0)
            {
                playerVelocity.y = 0f;
            }
        }
    }

    private class Move : MonoBehaviour
    {

    }

    private class Jump : MonoBehaviour
    {

    }

    private class Climb : MonoBehaviour
    {

    }

    
    private class WallRun : MonoBehaviour
    {

    }

    // OLD UPDATE
    private void Update()
    {
        // DONE =======================================
        *//*groundedPlayer = playerController.isGrounded;
        if(groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }*//*
        // ============================================

        // DONE ========================================
        //Vector3 move = new Vector3(movInput.x, 0, movInput.y);
        *//*Vector3 moveDirection = (movInput.y * transform.forward) + (movInput.x * transform.right);
        playerController.Move(moveDirection * Time.deltaTime * playerSpeed);*//*
        // ==============================================


        *//*if (moveDirection!= Vector3.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(moveDirection, Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
        }*//*

        // DONE =======================================================
        *//*// Changes the height position of the player
        if (hasJumped && groundedPlayer)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        playerController.Move(playerVelocity * Time.deltaTime);*//*
        // ============================================================

       // DONE ========================================================
        *//* // Mouse look rotation
        cameraTransform.localRotation = Quaternion.Euler(xRotation, 0, 0);
        this.transform.Rotate(Vector3.up * mouseX);*//*
        // ============================================================
    }

    *//*public void OnMove(InputAction.CallbackContext context)
    {
        movInput = context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        hasJumped = context.action.triggered;
    }

    public void OnMouseLook(InputAction.CallbackContext context)
    {
        mouseLook = context.ReadValue<Vector2>();
        mouseX = mouseLook.x * mouseSense * Time.deltaTime;
        float mouseY = mouseLook.y * mouseSense * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90);
    }*//*
    //=======================================================================================//


    // Move this into jump state - DONE
    *//*private void OnTriggerEnter(Collider other)
    {
        // If the player is in air check if its trying to grab the ledge, change into climb state
        if (other.gameObject.tag == "Ledge")
        {
            PlayerState.climbState;
            
            // Move this into climb state, restrict the controls
            // Stick to the ledge
            if (Input.GetKeyDown(KeyCode.Space))
            {
                // Jump State
            }
            else
            {
                // Sticks to ledges
                // Climbing State 
                transform.position = new Vector3(transform.position.x, other.transform.position.y, transform.position.z);
            }
        }
    }*//*
}*/

