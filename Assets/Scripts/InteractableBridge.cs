using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractableBridge : MonoBehaviour
{
    // Interactable object
    [SerializeField] private GameObject interactable;

    // Hint text shown to the player
    [SerializeField] private GameObject textCue;

    [SerializeField] private UnityEvent interactableActivated; 
    
    // Check if player is near the interactable object
    private bool playerIsNearby;

    public bool moveObject;


    void Start()
    {
        /*playerIsNearby = false;
        textCue.SetActive(false);*/
        moveObject = false;
    }

    void Update()
    {
        // TO-DO: Optimize later
        // Testing with PC Player

        if (playerIsNearby)
        {
            interactableActivated.Invoke();
        }

        if (moveObject)
        {
            MoveInteractableBridge(interactable);
        }
    }

    public void MoveInteractableBridge(GameObject bridgeObject)
    {
        if (bridgeObject.transform.rotation.eulerAngles.z > 1f)
        {
            bridgeObject.transform.Rotate(new Vector3(0, 0, -20) * Time.deltaTime);
        }
    }

    public void MoveInteractableTree(GameObject treeObject)
    {

    }

    private void OnTriggerEnter(Collider other)
    {

            Debug.Log("Player In Zone");
            playerIsNearby = true;
            //textCueSetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "PlayerVR")
        {

        }

        if (other.gameObject.tag == "PlayerPC") {
            Debug.Log("Player Not In Zone");
            playerIsNearby = false;
            //textCue.SetActive(false);
        }
    }

    public void IsInteractableActivated()
    {
        moveObject = true;
    }
}
