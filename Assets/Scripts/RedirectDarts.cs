using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedirectDarts : MonoBehaviour
{
    Vector3 dir;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        dir = Camera.main.transform.forward;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Dart")
        {
            Debug.Log("yes");
            collision.gameObject.GetComponent<Rigidbody>().velocity = (dir * 200);
        }
    }
}
